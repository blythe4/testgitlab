
# Parallax
> Parallax Scrolling 방법

### 1. backgrouond-image
>>>
 배경 이미지에 parallax 효과 적용하기   
 [demo](https://www.w3schools.com/howto/howto_css_parallax.asp)
 [scrollTop](https://www.codementor.io/lautiamkok/js-tips-creating-a-simple-parallax-scrolling-with-css3-and-jquery-efp9b2spn)
>>>

```css
.el{
    ...
    background-attachment: fixed;
    background-position: 0 0;
    ...
}
 ```
 ```javascript
 $(window).on('scroll',function(){
    var st = $(this).scrollTop();
    $(selector).css({'backgroundPosition':'0 -' + (st * .25) + 'px'});
 });
 ```

 ### 2. position
 > element에 position(fixed/absolute) 시킨 후 top 또는 translateY(translate3d)를 이용

 >>>
 [fixed Exemple](http://jonathannicol.com/projects/parallax-scrolling/)
 >>>

 ```css
.el{
    ...
    position: fixed;
    ...
}
 ```
 ```javascript
 $(window).on('scroll',function(){
    st = $(this).scrollTop();
    $(selector).css({'top':'-'+ st * .75 +'px)'});
    // or
    $(selector).css({'transform':'translateY(-'+ st * .75 +'px)'});
    // or
    $(selector).css({'transform':'translate3d(0, -'+ st * .75 +'px, 0)'});
 });
 ```