# GitLab 연동하기
1. git을 설치 한다.
2. VSCode에서 Git:Clone 후 리포지토리(HTTPS 주소) URL을 입력한다.
3. gitlab 저장소로 사용할 폴더를 생성하고 리포지토리 초기화를 누른다.
4. 생성한 폴더 안에 .git이라는 숨김폴더가 생긴다.
5. 파일 및 폴더를 생성하고 왼쪽 3번째 아이콘을 누르면 소스제어:GIT이 보이고 변경 내용에 수정하거나 추가된 파일들이 나타난다.
6. 원하는 파일 또는 모든 파일에 대해 스테이징을 한다.
7. 메시지에 내용을 적은 후 Commit한다.
8. Commit 후 Push 해주면 끝!